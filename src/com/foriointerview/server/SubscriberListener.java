package com.foriointerview.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by sathaye on 4/3/16.
 */
public class SubscriberListener extends Thread {
    PubSubServer pubSubServer = new PubSubServer();
    BufferedReader bufferedReader;
    String message;
    Socket listener=null;

    public SubscriberListener(Socket listener) {
        this.listener = listener;
    }

    public void run(){
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(listener.getInputStream()));
            PrintWriter dout = new PrintWriter(listener.getOutputStream(),true);
            message = bufferedReader.readLine();
            System.out.println(message);
            String[] messageFromSubscriber = message.split(" ");
            String response = pubSubServer.handleSubscription(messageFromSubscriber[0], messageFromSubscriber[1], messageFromSubscriber[2]);
            System.out.println(response);
            dout.println(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

