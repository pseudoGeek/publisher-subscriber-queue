package com.foriointerview.server;

import com.foriointerview.utils.Message;
import com.foriointerview.utils.MessageQueue;
import com.foriointerview.utils.Subscriber;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by sathaye on 4/4/16.
 */
public class UpdateCache extends Thread {
    final private MessageQueue messageQueue;
    private Message m;

    public UpdateCache(MessageQueue messageQueue, Message m) {
        this.messageQueue = messageQueue;
        this.m = m;
    }

    public void run() {
        Subscriber s;
        HashMap hashMap = messageQueue.subscriberList;
        Iterator iterator = hashMap.values().iterator();
        while (iterator.hasNext()) {
            s = (Subscriber) iterator.next();
            if (s.getSubState().equals("SUSPEND")) {
                synchronized (messageQueue) {
                    messageQueue.setSemaphore(messageQueue.getSemaphore() - 1);
                    if (messageQueue.getSemaphore() == 0) {
                        messageQueue.setSemaphore(messageQueue.subscriberList.size());
                        messageQueue.getPubsubQueue().remove();
                        messageQueue.notifyAll();
                    }
                    messageQueue.subscriberList.get(s.getID()).getTempQueue().add(m);
                }
            }
        }
    }
}