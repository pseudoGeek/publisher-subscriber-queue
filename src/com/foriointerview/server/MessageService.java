package com.foriointerview.server;

import com.foriointerview.utils.MessageQueue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by sathaye on 4/3/16.
 */
public class MessageService extends Thread {

    private int SERVERPORT;
    ServerSocket messageSender = null;
    Socket listener;
    MessageQueue messageQueue;

    public MessageService(MessageQueue messageQueue) throws IOException {
        this.messageQueue = messageQueue;
        this.SERVERPORT = messageQueue.getPortID();
        this.messageSender = new ServerSocket(SERVERPORT);
    }

    @Override
    public synchronized void run() {
        try {
            while (true) {
                listener = messageSender.accept();
                BufferedReader br = new BufferedReader(new InputStreamReader(listener.getInputStream()));
                //System.out.println("Sending message to new subscribers!");
                String subscriberID = br.readLine();
                Courier courier = new Courier(listener, messageQueue,subscriberID);
                courier.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}