package com.foriointerview.server;

import com.foriointerview.utils.MessageQueue;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by sathaye on 4/2/16.
 */
public class PublisherService extends Thread {
    PubSubServer pubSubServer = new PubSubServer();
    ServerSocket publisherListener = null;
    Socket listener;

    public PublisherService(int port) throws IOException {
        this.publisherListener = new ServerSocket(port);
    }

    @Override
    public void run() {
        while(true) {
            try {
                listener = publisherListener.accept();
                MessageQueue publisher = pubSubServer.addPublisher();
                PublisherListener publishers = new PublisherListener(listener,publisher);
                System.out.println("New publisher added with name "+publisher.getQueueName());
                publishers.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
