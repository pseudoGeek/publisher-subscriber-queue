package com.foriointerview.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by sathaye on 4/3/16.
 */
public class SubscriberService extends Thread {
    private int SERVERPORT;
    ServerSocket subscriberListener = null;
    Socket listener;

    public SubscriberService(int port) throws IOException {
        this.SERVERPORT = port;
        this.subscriberListener = new ServerSocket(SERVERPORT);
    }

    @Override
    public void run() {
        while(true) {
            try {
                listener = subscriberListener.accept();
                System.out.println("New Subscribers has connected!");
                SubscriberListener subscribers = new SubscriberListener(listener);
                subscribers.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
