package com.foriointerview.server;

import com.foriointerview.utils.MessageQueue;
import com.foriointerview.utils.Subscriber;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by sathaye on 4/2/16.
 */
public class PubSubServer {
    private static int publishersCount = 0;
    private int queuePorts = 9000;
    public static HashMap<Integer,Integer> subscriberCount = new HashMap();
    public static HashMap<String,MessageQueue> pubsubQueue = new HashMap<>();
    public static HashMap<String,Integer> pubPort = new HashMap<>();
    public static HashMap<String, List<String>> dictionary = new HashMap();

    //
//    //Adds a new subscriber.

    /**
     * Handles the subscription
     * @param command: Command for Unsubscribing or Subscribing to a channel
     * @param publisherString: Publisher name
     * @param subID: Unique subscriber ID for the connection.
     * @return string containing acknowledging message
     */

    public synchronized String handleSubscription(String command, String publisherString, String subID) {
        String status = null;
        switch (command) {
            case "Subscribe":
                //New subscriber
                if(subID.equals("null")) {
                    String UID = UUID.randomUUID().toString();
                    Subscriber newSub = new Subscriber(UID, "ALIVE");

                    if (pubsubQueue.containsKey(publisherString)) {
                        pubsubQueue.get(publisherString).subscriberList.put(UID, newSub);
                        pubsubQueue.get(publisherString).setSemaphore(pubsubQueue.get(publisherString).getSemaphore() + 1);
                    } else {
                        return "No Such Queue";
                    }

                    status = UID+" "+pubsubQueue.get(publisherString).getPortID()+" New subscription";
                }
                //Re-subscribing
                else if(pubsubQueue.get(publisherString).subscriberList.containsKey(subID)){
                    pubsubQueue.get(publisherString).subscriberList.get(subID).setSubState("ALIVE");
                    status = subID+" "+pubsubQueue.get(publisherString).getPortID()+" Resubscription";
                }
                break;
            case "Unsubscribe":
                pubsubQueue.get(publisherString).subscriberList.get(subID).setSubState("SUSPEND");
                //pubsubQueue.get(publisherString).setSemaphore(pubsubQueue.get(publisherString).getSemaphore()-1);
                return "You have been unsubsribed. To re-subscribe Send 'Subscribe <publisher name>'";

        }
        return status;
    }

    /**
     * Adds publisher into the system
     * @return returns a message queue for that publisher
     * @throws IOException
     */
    public synchronized MessageQueue addPublisher() throws IOException {
        String pubID = "publisher-"+publishersCount++;
        MessageQueue messageQueue = new MessageQueue(queuePorts++,pubID);
        pubsubQueue.put(pubID,messageQueue);
        startPublishing(messageQueue);
        return messageQueue;
    }

    /**
     * On registering a queue, start a thread that will put messages into the respective queue
     * @param messageQueue: MessageQueue associated with the publisher.
     * @throws IOException
     */
    private void startPublishing(MessageQueue messageQueue) throws IOException {
        System.out.println("Publisher " + messageQueue.getQueueName() + " Ready to publish!!");
        MessageService messageService = new MessageService(messageQueue);
        messageService.start();

    }

    /**
     * Starts a thread that will listen to potential subsribers and register them into the server.
     * @throws IOException
     */
    private void startSubscriberService() throws IOException {
        System.out.println("About to start Subscribers Service!");
        SubscriberService publisherService = new SubscriberService(8090);
        publisherService.start();
    }

    /**
     * Starts a thread that will listen to potential publishers and register them into the server.
     * @throws IOException
     */
    private void startPublisherService() throws IOException {
        PublisherService publisherService = new PublisherService(8080);
        publisherService.start();
    }

    /**
     * Main function
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        PubSubServer pubSubServer = new PubSubServer();
        pubSubServer.startPublisherService();
        System.out.println("Publisher Service Started");
        pubSubServer.startSubscriberService();
        System.out.println("Subscriber Service Started");
    }
}