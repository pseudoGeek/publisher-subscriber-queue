package com.foriointerview.server;

import com.foriointerview.utils.Message;
import com.foriointerview.utils.MessageQueue;
import com.foriointerview.utils.Subscriber;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by sathaye on 4/3/16.
 */
public class Courier extends Thread{
    PubSubServer pubSubServer = new PubSubServer();
    BufferedReader bufferedReader;
    Message message;
    private boolean read=false;

    Socket listener=null;
    final MessageQueue messageQueue;
    String subscriberID;

    public Courier(Socket listener,MessageQueue messageQueue,String subscriberID) {
        this.listener = listener;
        this.messageQueue = messageQueue;
        this.subscriberID = subscriberID;
    }

    public void run(){
        try {
            PrintWriter dout = new PrintWriter(listener.getOutputStream(), true);
            bufferedReader = new BufferedReader(new InputStreamReader(listener.getInputStream()));
            while(true) {
                while (messageQueue.getPubsubQueue().size() == 0 || read) {
                    read = false;
                    synchronized (messageQueue) {
                        messageQueue.wait();
                    }
                }

                message = messageQueue.getPubsubQueue().peek();
                read = true;
                synchronized (messageQueue) {
                    messageQueue.setSemaphore(messageQueue.getSemaphore() - 1);
                    if (messageQueue.getSemaphore() == 0 ){
                        messageQueue.setSemaphore(messageQueue.subscriberList.size());
                        messageQueue.getPubsubQueue().remove();
                        messageQueue.notifyAll();
                    }
                }
                Subscriber subscriber = messageQueue.subscriberList.get(subscriberID);
                switch (subscriber.getSubState()) {
                    case "ALIVE":
                        while (!subscriber.getTempQueue().isEmpty()) {
                            Message tempMessage = subscriber.getTempQueue().remove();
                            dout.println(tempMessage.getID() + " " + tempMessage.getTimeStamp() + " " + tempMessage.getMessageBody());
                            if (bufferedReader.readLine() == null) {
                                throw new NullPointerException();
                            }
                        }
                            dout.println(message.getID() + " " + message.getTimeStamp() + " " + message.getMessageBody());
                            if (bufferedReader.readLine() == null) {
                                throw new Exception();
                            }
                            break;
                            case "SUSPEND":
                                throw new Exception();
                        }
                }
            }catch (Exception e) {
            messageQueue.subscriberList.get(subscriberID).getTempQueue().add(message);
            pubSubServer.handleSubscription("Unsubscribe",messageQueue.getQueueName(),subscriberID);
            e.printStackTrace();
        } finally {
                try {
                    listener.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }