package com.foriointerview.server;

import com.foriointerview.utils.Message;
import com.foriointerview.utils.MessageQueue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Date;

/**
 * Created by sathaye on 4/2/16.
 */
public class PublisherListener extends Thread{
    BufferedReader bufferedReader;
    String message;
    Socket listener=null;
    final MessageQueue messageQueue;

    public PublisherListener(Socket listener,MessageQueue publisher) {
        this.listener = listener;
        this.messageQueue = publisher;
    }

    public void run(){
        try {

            bufferedReader = new BufferedReader(new InputStreamReader(listener.getInputStream()));
            while ((message = bufferedReader.readLine()) !=null) {
                long time = new Date().getTime();
                String ID = messageQueue.getQueueName()+String.valueOf(time);
                Message m = new Message(message,time,ID);
                messageQueue.getPubsubQueue().add(m);
                UpdateCache updateCache = new UpdateCache(messageQueue,m);
                updateCache.start();
                synchronized (messageQueue) {
                    messageQueue.notifyAll();
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
