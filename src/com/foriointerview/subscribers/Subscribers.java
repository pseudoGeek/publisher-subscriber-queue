package com.foriointerview.subscribers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by sathaye on 4/1/16.
 */
public class Subscribers{
    private static String UID=null;
    //System.out.println("You're now connected to the Server"); /*this should only print once */
    //Send the message to the server
    public static void main(String[] args) throws IOException {
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        String input = stdIn.readLine();
        String[] output = input.split(" ");
        String command = output[0];
        String pubID = output[1];
        if(output.length == 3)
            UID = output[2];
        String host = "localhost";
        int port = 8090;
        Socket connectionSocket = new Socket(host, port);
        PrintWriter dout = new PrintWriter(connectionSocket.getOutputStream(), true);
        BufferedReader nIn = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        dout.println(command + " " + pubID + " " + UID);
        String response = nIn.readLine();
        System.out.println(response);
        UID = response.split(" ")[0];
        int subscriptionPort = Integer.parseInt(response.split(" ")[1]);
        Socket messageSocket = new Socket(host, subscriptionPort);
        dout = new PrintWriter(messageSocket.getOutputStream(),true);
        dout.println(UID);
        connectionSocket.close();
        nIn = new BufferedReader(new InputStreamReader(messageSocket.getInputStream()));
        while((input = nIn.readLine())!=null){
            dout.println("I AM ALIVE");
            System.out.println(input);
        }
    }
}
