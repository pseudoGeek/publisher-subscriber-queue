package com.foriointerview.utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by sathaye on 4/3/16.
 */
public class MessageQueue {
    private int portID;
    private String queueName;
    private Queue<Message> pubsubQueue;
    private int semaphore;
    public HashMap<String,Subscriber> subscriberList;
    public int down=0;

    /**
     * Constructor for creating a messageQueue for a particular publisher.
     * @param portID
     * @param queueName
     */
    public MessageQueue(int portID,String queueName){
        this.portID = portID;
        this.queueName = queueName;
        pubsubQueue = new LinkedList<>();
        subscriberList = new HashMap<>();
        this.semaphore=0;
        this.down=0;
    }

    public int getPortID() {
        return portID;
    }

    public void setPortID(int portID) {
        this.portID = portID;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public Queue<Message> getPubsubQueue() {
        return pubsubQueue;
    }

    public void setPubsubQueue(Queue<Message> pubsubQueue) {
        this.pubsubQueue = pubsubQueue;
    }

    public int getSemaphore() {
        return semaphore;
    }

    public synchronized void setSemaphore(int semaphore) {
        this.semaphore = semaphore;
    }

    public int getDown() {
        return down;
    }

    public void setDown(int down) {
        this.down = down;
    }
}
