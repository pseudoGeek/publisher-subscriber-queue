package com.foriointerview.utils;

/**
 * Created by sathaye on 4/2/16.
 */
public class Message {
    private String ID;
    private String command;
    private String messageBody;
    private String timeStamp;

    public Message(String message, long timeStamp,String ID) {
        this.ID = ID;
        this.messageBody = message;
        this.timeStamp = String.valueOf(timeStamp);
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * Created by sathaye on 4/1/16.
     */

}