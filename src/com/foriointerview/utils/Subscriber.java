package com.foriointerview.utils;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by sathaye on 4/2/16.
 */
public class Subscriber {
    private String ID;
    private Queue<Message> tempQueue;
    private String subState;

    public Subscriber(String ID,String subState){
        this.ID = ID;
        this.subState = subState;
        tempQueue = new LinkedList();
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public Queue<Message> getTempQueue() {
        return tempQueue;
    }

    public void setTempQueue(Queue<Message> tempQueue) {
        this.tempQueue = tempQueue;
    }

    public String getSubState() {
        return subState;
    }

    public void setSubState(String subState) {
        this.subState = subState;
    }
}
