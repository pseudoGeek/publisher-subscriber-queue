package com.foriointerview.publishers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by sathaye on 4/2/16.
 */
public class Publishers {
    //System.out.println("You're now connected to the Server"); /*this should only print once */
    //Send the message to the server


    public static void main(String[] args) throws IOException {
        String host = "localhost";
        int port = 8080;
        InetAddress address = InetAddress.getByName(host);
        Socket socket = new Socket(address, port);
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter dout = new PrintWriter(socket.getOutputStream(),true);
        while(true) {
            String message = stdIn.readLine();
            dout.println(message);
        }
    }
}
