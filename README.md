README #

* Quick summary

An in-memory publish/subscribe message queue.

Clients address queues by a simple string. Subscribing to a queue provides a unique identifier, and re-subscribing with that identifier guarantees delivery of all messages that client has not yet seen since the original subscription.


Messages themselves are simple strings, and include a unique message identifier and the timestamp at which they were published.

### Known Issues ###

When a particular subscriber resubsribers using the same subscriber ID then all the messages buffered in the queue for that subscriber would be eventually delivered after there is another message is sent by the publisher

![pubsubqueue.png](https://bitbucket.org/repo/BE4n8r/images/2873589357-pubsubqueue.png)#